#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int f_1(int value)
{
	return 10*value;
}

int f_2(int value)
{
	return 11*value;
}

int f_3(int value)
{
	return 12*value;
}
typedef int(*px)(int);
int main()
{
    px *pointer=NULL;
    pointer = (px*)calloc(3, sizeof(px));
    pointer[0] = f_1;
    pointer[1] = f_2;
    pointer[2] = f_3;
       
    for (int i = 0; i < 3; i++)
    {
       printf("%d\n", pointer[i](i+10));
    }
	
_getch();
}